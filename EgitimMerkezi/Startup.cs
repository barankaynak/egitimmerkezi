﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EgitimMerkezi.Startup))]
namespace EgitimMerkezi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
